import Vue from "vue";
import Vuex from "vuex";
import {
    INCR_CURRENT_PAGE,
    SET_BIOMETRICS,
    SET_MANUFACTURERS,
    SET_SCREENSIZE,
    SET_STORAGE_SIZE,
    SET_WATERPROOFING
} from "./mutationTypes";

Vue.use(Vuex);

export const ANY = Symbol("any");

export default new Vuex.Store({
    state: {
        currentPage: 0,
        manufacturer: ANY,
        screenSize: ANY,
        storage: ANY,
        waterproofing: ANY,
        biometrics: ANY
    },
    getters: {
        choices: state => {
            return {
                manufacturer: state.manufacturer,
                screenSize: state.screenSize,
                storage: state.storage,
                waterproofing: state.waterproofing,
                biometrics: state.biometrics
            }
        }
    },
    mutations: {
        [SET_MANUFACTURERS](state, data) {
            state.manufacturer = data;
        },
        [INCR_CURRENT_PAGE](state) {
            state.currentPage += 1;
        },
        [SET_SCREENSIZE](state, data) {
            state.screenSize = data
        },
        [SET_STORAGE_SIZE](state, data) {
            state.storage = data
        },
        [SET_WATERPROOFING](state, data) {
            state.waterproofing = data
        },
        [SET_BIOMETRICS](state, data) {
            state.biometrics = data
        }
    },
    actions: {},
    modules: {}
});
