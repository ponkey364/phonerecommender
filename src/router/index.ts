import Vue from "vue";
import VueRouter, {RouteConfig} from "vue-router";

import Home from "../views/Home.vue";
import Start from "../views/Start.vue"

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/About.vue")
    },
    {
        path: "/start",
        name: "Start",
        component: Start
    },
    {
        path: "/steps",
        name: "Steps",
        component: () =>
            import(/* webpackChunkName: "steps" */ "../views/steps/Index.vue"),
        children: [
            {
                name: "Step 1",
                path: "1",
                component: () => import(/* webpackChunkName: "steps_simple" */ "../views/steps/Step1.vue"),
            },
            {
                name: "Step 2",
                path: "2",
                component: () => import(/* webpackChunkName: "steps_simple" */ "../views/steps/Step2.vue"),
            },
            {
                name: "Step 3",
                path: "3",
                component: () => import(/* webpackChunkName: "steps_simple" */ "../views/steps/Step3.vue"),
            },
            {
                name: "Step 4",
                path: "4",
                component: () => import(/* webpackChunkName: "steps_simple" */ "../views/steps/Step4.vue"),
            },
            {
                name: "Step 5",
                path: "5",
                component: () => import(/* webpackChunkName: "steps_simple" */ "../views/steps/Step5.vue"),
            }
        ]
    },
    {
        name: "Results",
        path: "/results",
        component: () => import("../views/Results.vue"),
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

export default router;
