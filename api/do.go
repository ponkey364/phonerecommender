package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"time"
)

type BodyData struct {
	Manufacturer  *[]string
	Biometrics    *[]uint
	ScreenSize    *[]float32
	Storage       *[]uint
	Waterproofing *uint
}

func Handler(w http.ResponseWriter, r *http.Request) {
	bytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
		http.Error(w, "error reading body", http.StatusInternalServerError)
		return
	}

	var body *BodyData
	err = json.Unmarshal(bytes, &body)
	if err != nil {
		fmt.Println(err)
		http.Error(w, "error parsing body", http.StatusInternalServerError)
		return
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb+srv://%s:%s@%s/test?retryWrites=true&w=majority", os.Getenv("ATL_USER"), os.Getenv("ATL_PASS"), os.Getenv("ATL_HOST"))))
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	filter := bson.D{
		{
			"Retired",
			false,
		},
	}

	// When checking for filters, we check if stuff is nil because for some reason, Symbol("ANY") isn't converted to
	// any form of JSON data and the field ends up being nil when go's JSON parser reads it

	if body.Manufacturer != nil {
		filter = append(filter, bson.E{
			Key: "Manufacturer",
			Value: bson.D{{
				"$in",
				*body.Manufacturer},
			},
		})
	}

	if body.ScreenSize != nil {
		filter = append(filter, bson.E{
			Key: "$and",
			Value: bson.A{
				bson.D{{
					"Screen Size",
					bson.D{{
						"$gte",
						(*body.ScreenSize)[0],
					}},
				}},
				bson.D{{
					"Screen Size",
					bson.D{{
						"$lte",
						(*body.ScreenSize)[1],
					}},
				}},
			},
		})
	}

	if body.Waterproofing != nil {
		switch *body.Waterproofing {
		case 0:
			{
				filter = append(filter, bson.E{
					Key:   "IP Rating",
					Value: nil,
				})
			}
		case 2:
			{
				filter = append(filter, bson.E{
					Key: "IP Rating",
					Value: bson.D{{
						"$in",
						[]string{"IP53"},
					}},
				})
			}
		case 3:
			{
				filter = append(filter, bson.E{
					Key: "IP Rating",
					Value: bson.D{{
						"$in",
						[]string{"IP67", "IP68"},
					}},
				})
			}
		default:
			break
		}
	}

	if body.Storage != nil {
		// Storage is *very* fun, we're given a pair of numbers between 0 and 6, and we're expected to add 4 to the
		// number and pow it for the amnt. of storage (treating 1TB = 1024GB, which is patently false, but 2^10 is 1024
		// so that's what i'll use)
		filter = append(filter, bson.E{
			Key: "Storage",
			Value: bson.D{{
				"$elemMatch",
				bson.M{
					"$gte": uint32(math.Pow(2, float64((*body.Storage)[0]+4))),
					"$lte": uint32(math.Pow(2, float64((*body.Storage)[1]+4))),
				},
			}},
		})
	}

	if body.Biometrics != nil {
		if SliceIndex(*body.Biometrics, 1) != -1 {
			filter = append(filter, bson.E{
				Key: "Facial Recognition",
				Value: bson.D{{
					"$eq",
					true,
				}},
			})
		}
		if SliceIndex(*body.Biometrics, 2) != -1 {
			filter = append(filter, bson.E{
				Key: "Fingerprint Sensor",
				Value: bson.D{{
					"$eq",
					true,
				}},
			})
		}
	}

	cur, err := client.Database("pr").Collection("pr").Find(context.Background(), filter)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	results := []bson.M{}
	if err = cur.All(context.TODO(), &results); err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(results)
}

func SliceIndex(slice []uint, want uint) int {
	for i, v := range slice {
		if v == want {
			return i
		}
	}
	return -1
}
