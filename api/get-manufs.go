package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/http"
	"os"
	"time"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb+srv://%s:%s@%s/test?retryWrites=true&w=majority", os.Getenv("ATL_USER"), os.Getenv("ATL_PASS"), os.Getenv("ATL_HOST"))))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	filter := bson.D{}
	opts := options.Distinct()
	data, err := client.Database("pr").Collection("pr").Distinct(context.TODO(), "Manufacturer", filter, opts)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
